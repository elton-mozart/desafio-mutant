const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const indexRouter = require("./routes/indexRouter");

const PORT = 3000;
const HOST = "0.0.0.0";

const app = express();

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));

app.use("/", indexRouter);

app.listen(PORT, HOST);
