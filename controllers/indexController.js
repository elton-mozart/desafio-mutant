const axios = require("axios");

const home = (req, res) => {
  res.render("index");
};

const getDesafio1Data = async () => {
  const data = await axios.get("https://jsonplaceholder.typicode.com/users");
  const websites = data.data.map(usuario => {
    return usuario.website;
  });
  return websites;
};

const desafio1 = async (req, res) => {
  const websites = await getDesafio1Data();
  res.render("desafio1", { websites });
};

const getDesafio2Data = async () => {
  const data = await axios.get("https://jsonplaceholder.typicode.com/users");
  const result = data.data.map(usuario => {
    return {
      nome: usuario.name,
      email: usuario.email,
      empresa: usuario.company.name
    };
  });

  result.sort((user1, user2) => {
    return user1.nome < user2.nome ? -1 : user1.nome > user2.nome ? 1 : 0;
  });

  return result;
};

const desafio2 = async (req, res) => {
  const result = await getDesafio2Data();
  res.render("desafio2", { result });
};

const getDesafio3Data = async () => {
  const data = await axios.get("https://jsonplaceholder.typicode.com/users");
  const usuarios = data.data.filter(usuario => {
    return usuario.address.suite.indexOf("Suite") > -1;
  });
  return usuarios;
};

const desafio3 = async (req, res) => {
  const usuarios = await getDesafio3Data();
  res.render("desafio3", { usuarios });
};

module.exports = {
  home,
  desafio1,
  desafio2,
  desafio3,
  getDesafio1Data,
  getDesafio2Data,
  getDesafio3Data
};
