const indexController = require("./indexController");

describe("Testes do Desafio Mutant", () => {
  it("Teste do Desafio 1: Websites dos usuários.", async () => {
    const websites = await indexController.getDesafio1Data();

    expect(websites.length).toBe(10);
  });

  it("Teste do Desafio 2: O Nome, email e a empresa em que trabalha (em ordem alfabética)", async () => {
    const usuarios = await indexController.getDesafio2Data();
    expect(usuarios.length).toBe(10);
  });

  it("Teste do Desafio 3: usuários que contem a palavra suite no endereço.", async () => {
    const usuarios = await indexController.getDesafio3Data();
    expect(usuarios.length).toBe(7);
  });
});
