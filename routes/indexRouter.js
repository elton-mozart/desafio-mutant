const express = require("express");
const router = express.Router();
const indexController = require("../controllers/indexController");

router.get("/", indexController.home);
router.get("/desafio1", indexController.desafio1);
router.get("/desafio2", indexController.desafio2);
router.get("/desafio3", indexController.desafio3);

module.exports = router;
